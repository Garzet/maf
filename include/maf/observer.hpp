#ifndef MAF_OBSERVER_HPP
#define MAF_OBSERVER_HPP

#include "maf/endpoint.hpp"

#include <string>

namespace maf {

class ComponentObserver
{
  public:
    virtual ~ComponentObserver() noexcept = default;

    virtual void on_accept_registered(const TcpEndpoint& listen_endpoint) noexcept                                 = 0;
    virtual void on_accept_success(const TcpEndpoint& peer_endpoint) noexcept                                      = 0;
    virtual void on_connection_from_unexpected_endpoint(const TcpEndpoint& peer_endpoint) noexcept                 = 0;
    virtual void on_connection_identified(const std::string& peer_name, const TcpEndpoint& peer_endpoint) noexcept = 0;
    virtual void on_multiple_connects_from_same_peer(const std::string& peer_name,
                                                     const TcpEndpoint& peer_endpoint) noexcept                    = 0;

    virtual void on_connect_registered(const std::string& peer_name, const TcpEndpoint& peer_endpoint) noexcept = 0;
    virtual void on_connect_success(const std::string& peer_name, const TcpEndpoint& peer_endpoint) noexcept    = 0;

  protected:
    ComponentObserver() noexcept                                          = default;
    ComponentObserver(const ComponentObserver& other) noexcept            = default;
    ComponentObserver(ComponentObserver&& other) noexcept                 = default;
    ComponentObserver& operator=(const ComponentObserver& other) noexcept = default;
    ComponentObserver& operator=(ComponentObserver&& other) noexcept      = default;
};

} // namespace maf

#endif
