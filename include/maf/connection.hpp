#ifndef MAF_CONNECTION_HPP
#define MAF_CONNECTION_HPP

#include <boost/asio.hpp>

#include <array>
#include <string>
#include <system_error>

namespace maf {

class Connection final
{
  public:
    using TcpSocket   = boost::asio::ip::tcp::socket;
    using TcpEndpoint = boost::asio::ip::tcp::endpoint;

  private:
    TcpSocket socket;

    static constexpr int body_size_encode_base{16};
    static constexpr std::size_t header_size{8};

  public:
    explicit Connection(TcpSocket socket) noexcept;

    [[nodiscard]] std::string read();
    void write(const std::string& message);

    [[nodiscard]] const TcpSocket& get_socket() const noexcept;

  private:
    [[nodiscard]] std::size_t decode_message_size(std::array<char, header_size> header);
    [[nodiscard]] std::array<char, header_size> encode_message_size(std::size_t outbound_message_size);
};

class ConnectionError final : public std::system_error
{
  private:
    std::reference_wrapper<Connection> connection;

  public:
    ConnectionError(Connection& connection, std::error_code error_code) noexcept;

    [[nodiscard]] const Connection& get_connection() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ConnectionError>);

} // namespace maf

#endif
