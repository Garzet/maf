#ifndef MAF_OPERATION_HPP
#define MAF_OPERATION_HPP

#include <type_traits>

namespace maf {

template<typename T, typename... U>
constexpr bool is_any_of = (false || ... || std::is_same_v<T, U>);

struct Subtract;
struct Divide;

struct Add final
{
    using Inverse = Subtract;

    template<typename T>
    [[nodiscard]] constexpr T operator()(T lhs, T rhs) const noexcept
    {
        return lhs + rhs;
    }
};

struct Subtract final
{
    using Inverse = Add;

    template<typename T>
    [[nodiscard]] constexpr T operator()(T lhs, T rhs) const noexcept
    {
        return lhs - rhs;
    }
};

struct Multiply final
{
    using Inverse = Divide;

    template<typename T>
    [[nodiscard]] constexpr T operator()(T lhs, T rhs) const noexcept
    {
        return lhs * rhs;
    }
};

struct Divide final
{
    using Inverse = Multiply;

    template<typename T>
    [[nodiscard]] constexpr T operator()(T lhs, T rhs) const noexcept
    {
        return lhs / rhs;
    }
};

template<typename T>
constexpr bool is_arithmetic_operation = is_any_of<T, Add, Subtract, Multiply, Divide>;

template<typename T>
constexpr bool is_arithmetic_operand = is_any_of<T, int, double>;

struct Less final
{
    template<typename T>
    [[nodiscard]] constexpr bool operator()(T lhs, T rhs) const noexcept
    {
        return lhs < rhs;
    }
};

struct LessOrEqual final
{
    template<typename T>
    [[nodiscard]] constexpr bool operator()(T lhs, T rhs) const noexcept
    {
        return lhs <= rhs;
    }
};

struct Greater final
{
    template<typename T>
    [[nodiscard]] constexpr bool operator()(T lhs, T rhs) const noexcept
    {
        return lhs > rhs;
    }
};

struct GreaterOrEqual final
{
    template<typename T>
    [[nodiscard]] constexpr bool operator()(T lhs, T rhs) const noexcept
    {
        return lhs >= rhs;
    }
};

template<typename T>
constexpr bool is_comparison_operation = is_any_of<T, Less, LessOrEqual, Greater, GreaterOrEqual>;

template<typename T>
constexpr bool is_comparison_masking_operation = is_arithmetic_operation<T>;

template<typename T>
constexpr bool is_comparison_operand = is_any_of<T, int, double>;

struct Equal final
{
    template<typename T>
    [[nodiscard]] constexpr bool operator()(T lhs, T rhs) const noexcept
    {
        return lhs == rhs;
    }
};

struct NotEqual final
{
    template<typename T>
    [[nodiscard]] constexpr bool operator()(T lhs, T rhs) const noexcept
    {
        return lhs != rhs;
    }
};

template<typename T>
constexpr bool is_equality_operation = is_any_of<T, Equal, NotEqual>;

template<typename T>
constexpr bool is_salt_operation = is_arithmetic_operation<T>;

template<typename T>
constexpr bool is_equality_operand = is_any_of<T, int, double>;

template<typename T, typename Data>
constexpr bool is_hash_operation = std::is_nothrow_invocable_r_v<std::size_t, T, Data>;

} // namespace maf

#endif
