#ifndef MAF_ENDPOINT_HPP
#define MAF_ENDPOINT_HPP

#include <boost/asio/ip/tcp.hpp>
#include <fmt/format.h>

namespace maf {

using IpAddress   = boost::asio::ip::address;
using Port        = boost::asio::ip::port_type;
using TcpEndpoint = boost::asio::ip::tcp::endpoint;

} // namespace maf

template<>
class fmt::formatter<maf::IpAddress> final
{
  private:
    fmt::formatter<unsigned char> uchar_fmt;

  public:
    constexpr fmt::format_parse_context::iterator parse(fmt::format_parse_context& ctx)
    {
        return uchar_fmt.parse(ctx);
    }

    fmt::appender format(const maf::IpAddress& address, fmt::format_context& ctx)
    {
        if (address.is_v4()) {
            const auto bytes = address.to_v4().to_bytes();
            const auto* i    = bytes.begin();
            uchar_fmt.format(*i, ctx);
            for (++i; i != bytes.end(); ++i) {
                fmt::format_to(ctx.out(), ".");
                uchar_fmt.format(*i, ctx);
            }
        } else {
            fmt::format_to(ctx.out(), "[");
            const auto bytes = address.to_v6().to_bytes();
            const auto* i    = bytes.begin();
            uchar_fmt.format(*i, ctx);
            uchar_fmt.format(*i + 1, ctx);
            for (i += 2; i != bytes.end(); i += 2) {
                fmt::format_to(ctx.out(), ":");
                uchar_fmt.format(*i, ctx);
                uchar_fmt.format(*i + 1, ctx);
            }
            fmt::format_to(ctx.out(), "]");
        }
        return ctx.out();
    }
};

// Formatter for TCP endpoint objects that has the ability to control format of the IP address and the TCP port. For
// example, format specification "{:x:d}" formats IP address bytes in base 16 while port is formatted in base 10 (the
// TCP address formatter receives "x" as format specification while the port formatter receives "x" as the format
// specification).
template<>
class fmt::formatter<maf::TcpEndpoint> final
{
  private:
    fmt::formatter<maf::IpAddress> ip_address_fmt;
    fmt::formatter<maf::Port> port_fmt;

  public:
    constexpr fmt::format_parse_context::iterator parse(fmt::format_parse_context& ctx)
    {
        const auto [address_spec, port_spec] = split_format_specs(ctx);

        {
            fmt::format_parse_context address_ctx{address_spec};
            ip_address_fmt.parse(address_ctx);
        }

        {
            fmt::format_parse_context port_ctx{port_spec};
            port_fmt.parse(port_ctx);
        }

        return ctx.begin();
    }

    fmt::appender format(const maf::TcpEndpoint& endpoint, fmt::format_context& ctx)
    {
        ip_address_fmt.format(endpoint.address(), ctx);
        fmt::format_to(ctx.out(), ":");
        port_fmt.format(endpoint.port(), ctx);
        return ctx.out();
    }

  private:
    [[nodiscard]] static constexpr std::array<std::string_view, 2>
        split_format_specs(fmt::format_parse_context& ctx) noexcept
    {
        std::array<std::string_view, 2> result;

        constexpr char delimiter{':'};
        const auto* it = ctx.begin();

        {
            std::size_t address_ctx_size{0};
            for (; it != ctx.end() && *it != '}' && *it != delimiter; ++it) { ++address_ctx_size; }
            result[0] = std::string_view{ctx.begin(), address_ctx_size};
        }

        if (it != ctx.end() && *it != '}' && *it == delimiter) {
            ++it;
            std::size_t port_ctx_size{0};
            const auto* const port_begin = it;
            for (; it != ctx.end() && *it != '}'; ++it) { ++port_ctx_size; }
            result[1] = std::string_view{port_begin, port_ctx_size};
        }

        ctx.advance_to(it);

        return result;
    }
};

#endif
