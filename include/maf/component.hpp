#ifndef MAF_COMPONENT_HPP
#define MAF_COMPONENT_HPP

#include "maf/configuration.hpp"
#include "maf/protocol.hpp"

#include <cassert>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

namespace maf {

class Component
{
  protected:
    // Bring operations into scope so inheriting classes may use them without the "maf" namespace.
    using Add            = maf::Add;
    using Subtract       = maf::Subtract;
    using Multiply       = maf::Multiply;
    using Divide         = maf::Divide;
    using Less           = maf::Less;
    using LessOrEqual    = maf::LessOrEqual;
    using Greater        = maf::Greater;
    using GreaterOrEqual = maf::GreaterOrEqual;
    using Equal          = maf::Equal;
    using NotEqual       = maf::NotEqual;

  private:
    using ErrorCode   = boost::system::error_code;
    using IoContext   = boost::asio::io_context;
    using TcpAcceptor = boost::asio::ip::tcp::acceptor;
    using TcpSocket   = boost::asio::ip::tcp::socket;

  private:
    IoContext io_context;
    Configuration configuration;
    std::unordered_map<std::string, Connection> connections;
    RandomContext random_context;

  public:
    explicit Component(Configuration configuration);
    virtual ~Component() noexcept                         = default;
    Component(const Component& other) noexcept            = delete;
    Component(Component&& other) noexcept                 = delete;
    Component& operator=(const Component& other) noexcept = delete;
    Component& operator=(Component&& other) noexcept      = delete;

    void run();

  protected:
    virtual void on_all_connections_established() = 0;

    template<typename ArithmeticOperation, typename Data>
    void perform_arithmetic_protocol_initial(Data data,
                                             const std::string& second_component_name,
                                             const std::string& final_component_name);

    template<typename ArithmeticOperation, typename Data>
    void perform_arithmetic_protocol_intermediate(Data data,
                                                  const std::string& previous_component_name,
                                                  const std::string& next_component_name);

    template<typename ArithmeticOperation, typename Data>
    [[nodiscard]] Data perform_arithmetic_protocol_final(const std::string& initial_component_name,
                                                         const std::string& previous_component_name);

    template<typename Data, typename MaskingOperation = Add>
    void perform_comparison_protocol_initial(Data data,
                                             const std::string& second_component_name,
                                             const std::string& final_component_name);

    template<typename Data, typename MaskingOperation = Add>
    void perform_comparison_protocol_intermediate(Data data,
                                                  const std::string& initial_component_name,
                                                  const std::string& final_component_name);

    template<typename ComparisonOperation, typename Data>
    [[nodiscard]] bool perform_comparison_protocol_final(const std::string& initial_component_name,
                                                         const std::string& intermediate_component_name);

    template<typename Data, typename Hash = std::hash<Data>, typename SaltOperation = Add>
    void perform_equality_protocol_initial(Data data,
                                           const std::string& second_component_name,
                                           const std::string& final_component_name);

    template<typename Data, typename Hash = std::hash<Data>, typename SaltOperation = Add>
    void perform_equality_protocol_intermediate(Data data,
                                                const std::string& initial_component_name,
                                                const std::string& final_component_name);

    template<typename EqualityOperation>
    [[nodiscard]] bool perform_equality_protocol_final(const std::string& initial_component_name,
                                                       const std::string& intermediate_component_name);

    [[nodiscard]] bool write_and_return(bool value, const std::vector<std::string>& components);

  private:
    [[nodiscard]] Connection& get_connection(const std::string& component);

    void async_accept(std::shared_ptr<TcpAcceptor> acceptor) noexcept;

    void on_accept_success(std::shared_ptr<TcpAcceptor> acceptor, TcpSocket socket);

    void async_connect(std::size_t accepting_component_index) noexcept;
    void on_connect_success(std::shared_ptr<TcpSocket> socket, std::size_t accepting_component_index);

    [[nodiscard]] bool all_connections_established() const noexcept;

    [[nodiscard]] const std::string* identify_connected_component(const TcpEndpoint& peer_endpoint) const;

    [[nodiscard]] ComponentObserver* get_observer() const noexcept;
};

class MissingConnectionError final : public std::runtime_error
{
  public:
    explicit MissingConnectionError(std::string_view missing_component) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingConnectionError>);

class AcceptError final : public std::system_error
{
  public:
    using std::system_error::system_error;
    using std::system_error::operator=;
};
static_assert(std::is_nothrow_copy_constructible_v<AcceptError>);

class ConnectError final : public std::system_error
{
  public:
    using std::system_error::system_error;
    using std::system_error::operator=;
};
static_assert(std::is_nothrow_copy_constructible_v<ConnectError>);

template<typename ArithmeticOperation, typename Data>
void Component::perform_arithmetic_protocol_initial(Data data,
                                                    const std::string& second_component_name,
                                                    const std::string& final_component_name)
{
    assert(second_component_name != final_component_name);

    protocol::arithmetic::perform_initial<ArithmeticOperation, Data>(
        data, get_connection(second_component_name), get_connection(final_component_name), random_context);
}

template<typename ArithmeticOperation, typename Data>
void Component::perform_arithmetic_protocol_intermediate(Data data,
                                                         const std::string& previous_component_name,
                                                         const std::string& next_component_name)
{
    assert(previous_component_name != next_component_name);

    protocol::arithmetic::perform_intermediate<ArithmeticOperation, Data>(
        data, get_connection(previous_component_name), get_connection(next_component_name));
}

template<typename ArithmeticOperation, typename Data>
Data Component::perform_arithmetic_protocol_final(const std::string& initial_component_name,
                                                  const std::string& previous_component_name)
{
    assert(initial_component_name != previous_component_name);

    return protocol::arithmetic::perform_final<ArithmeticOperation, Data>(get_connection(initial_component_name),
                                                                          get_connection(previous_component_name));
}

template<typename Data, typename MaskingOperation>
void Component::perform_comparison_protocol_initial(Data data,
                                                    const std::string& second_component_name,
                                                    const std::string& final_component_name)
{
    assert(second_component_name != final_component_name);

    protocol::comparison::perform_initial<Data, MaskingOperation>(
        data, get_connection(second_component_name), get_connection(final_component_name), random_context);
}

template<typename Data, typename MaskingOperation>
void Component::perform_comparison_protocol_intermediate(Data data,
                                                         const std::string& initial_component_name,
                                                         const std::string& final_component_name)
{
    assert(initial_component_name != final_component_name);

    protocol::comparison::perform_intermediate<Data, MaskingOperation>(
        data, get_connection(initial_component_name), get_connection(final_component_name));
}

template<typename ComparisonOperation, typename Data>
bool Component::perform_comparison_protocol_final(const std::string& initial_component_name,
                                                  const std::string& intermediate_component_name)
{
    assert(initial_component_name != intermediate_component_name);

    return protocol::comparison::perform_final<ComparisonOperation, Data>(get_connection(initial_component_name),
                                                                          get_connection(intermediate_component_name));
}

template<typename Data, typename Hash, typename SaltOperation>
void Component::perform_equality_protocol_initial(Data data,
                                                  const std::string& second_component_name,
                                                  const std::string& final_component_name)
{
    assert(second_component_name != final_component_name);

    protocol::equality::perform_initial<Data, Hash, SaltOperation>(
        data, get_connection(second_component_name), get_connection(final_component_name), random_context);
}

template<typename Data, typename Hash, typename SaltOperation>
void Component::perform_equality_protocol_intermediate(Data data,
                                                       const std::string& initial_component_name,
                                                       const std::string& final_component_name)
{
    assert(initial_component_name != final_component_name);

    protocol::equality::perform_intermediate<Data, Hash, SaltOperation>(
        data, get_connection(initial_component_name), get_connection(final_component_name));
}

template<typename EqualityOperation>
bool Component::perform_equality_protocol_final(const std::string& initial_component_name,
                                                const std::string& intermediate_component_name)
{
    assert(initial_component_name != intermediate_component_name);

    return protocol::equality::perform_final<EqualityOperation>(get_connection(initial_component_name),
                                                                get_connection(intermediate_component_name));
}

} // namespace maf

#endif
