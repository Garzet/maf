#ifndef MAF_ENTRY_POINT_HPP
#define MAF_ENTRY_POINT_HPP

#include "maf/configuration.hpp"
#include "maf/options.hpp"

#include <fmt/format.h>

namespace maf {

template<typename Component>
void entry_point(int argc, char** argv, StaticConfigurationData static_config)
{
    CommandLineOptionsDescription command_line_options_description;
    const auto parsed_command_line = command_line_options_description.parse_command_line(argc, argv);
    if (parsed_command_line.is_flag_option_present("help")) {
        fmt::print(command_line_options_description.format_help_string(parsed_command_line.get_executable_name()));
        return;
    }
    Component component{Configuration{std::move(static_config), load_dynamic_configuration_data(parsed_command_line)}};
    component.run();
}

} // namespace maf

#endif
