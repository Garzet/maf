#ifndef MAF_SERIALIZATION_HPP
#define MAF_SERIALIZATION_HPP

#include <sstream>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/array.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

namespace maf {

template<typename T>
T deserialize(const std::string& serialized_data)
{
    T data;
    {
        std::istringstream archive_stream(serialized_data);
        boost::archive::binary_iarchive archive(archive_stream);
        archive >> data;
    }
    return data;
}

/* Serializes provided data. */
template<typename T>
std::string serialize(const T& data)
{
    std::ostringstream data_archive_stream;
    {
        boost::archive::binary_oarchive data_archive(data_archive_stream);
        data_archive << data;
    }
    return data_archive_stream.str();
}

} // namespace maf

#endif
