#ifndef MAF_RANDOM_HPP
#define MAF_RANDOM_HPP

#include <optional>
#include <random>

namespace maf {

class RandomContext final
{
  private:
    std::mt19937 random_generator;
    std::uniform_int_distribution<int> int_distribution;
    std::uniform_real_distribution<double> real_distribution;

  public:
    RandomContext(std::uniform_int_distribution<int> int_distribution,
                  std::uniform_real_distribution<double> real_distribution,
                  std::optional<unsigned> random_seed = std::nullopt) noexcept;

    template<typename T>
    [[nodiscard]] T generate() noexcept;
};

} // namespace maf

#endif
