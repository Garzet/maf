#include <boost/test/unit_test.hpp>

#include "maf/operation.hpp"

BOOST_AUTO_TEST_SUITE(operation);

BOOST_AUTO_TEST_CASE(add)
{
    static_assert(maf::Add{}(10, 20) == 30);
    static_assert(maf::Add::Inverse{}(10, 15) == -5);
}

BOOST_AUTO_TEST_CASE(subtract)
{
    static_assert(maf::Subtract{}(10, 20) == -10);
    static_assert(maf::Subtract::Inverse{}(10, 15) == 25);
}

BOOST_AUTO_TEST_SUITE_END();
