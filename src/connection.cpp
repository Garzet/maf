#include "maf/connection.hpp"

#include <cassert>
#include <charconv>
#include <numeric>

namespace maf {

Connection::Connection(TcpSocket socket) noexcept : socket{std::move(socket)}
{
    assert(this->socket.is_open());
}

std::string Connection::read()
{
    std::array<char, header_size> header{};

    { // Read header.
        boost::system::error_code error_code;
        const auto bytes_transferred = boost::asio::read(socket, boost::asio::buffer(header), error_code);
        if (error_code) { throw ConnectionError{*this, error_code}; }
        assert(bytes_transferred == header.size());
    }

    std::string message;
    message.resize(decode_message_size(header));

    { // Read message.
        boost::system::error_code error_code;
        const auto bytes_transferred = boost::asio::read(socket, boost::asio::buffer(message), error_code);
        if (error_code) { throw ConnectionError{*this, error_code}; }
        assert(bytes_transferred == message.size());
    }

    return message;
}

void Connection::write(const std::string& message)
{
    const auto header = encode_message_size(message.size());

    const std::array<boost::asio::const_buffer, 2> buffers{boost::asio::buffer(header), boost::asio::buffer(message)};

    boost::system::error_code error_code;
    const auto bytes_transferred = boost::asio::write(socket, buffers, error_code);
    if (error_code) { throw ConnectionError{*this, error_code}; }
    assert(bytes_transferred
           == std::accumulate(buffers.cbegin(),
                              buffers.cend(),
                              std::size_t{0},
                              [](std::size_t sum, boost::asio::const_buffer buffer) noexcept -> std::size_t {
                                  return sum + buffer.size();
                              }));
}

auto Connection::get_socket() const noexcept -> const TcpSocket&
{
    return socket;
}

std::size_t Connection::decode_message_size(std::array<char, header_size> header)
{
    std::size_t ret{0};
    const auto [ptr, ec] = std::from_chars(header.data(), header.data() + header_size, ret, body_size_encode_base);
    if (ec != std::errc{}) { throw ConnectionError{*this, std::make_error_code(ec)}; }
    return ret;
}

auto Connection::encode_message_size(std::size_t outbound_message_size) -> std::array<char, header_size>
{
    std::array<char, header_size> header{};
    const auto [ptr, ec] =
        std::to_chars(header.data(), header.data() + header_size, outbound_message_size, body_size_encode_base);
    if (ec != std::errc{}) { throw ConnectionError{*this, std::make_error_code(ec)}; }
    return header;
}

ConnectionError::ConnectionError(Connection& connection, std::error_code error_code) noexcept
    : std::system_error{error_code}, connection{connection}
{}

const Connection& ConnectionError::get_connection() const noexcept
{
    return connection;
}

} // namespace maf
