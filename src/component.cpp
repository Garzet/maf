#include "maf/component.hpp"

#include <fmt/format.h>

#include <chrono>
#include <memory>
#include <thread>

namespace maf {

Component::Component(Configuration configuration)
    : configuration{std::move(configuration)},
      random_context{std::uniform_int_distribution<int>{this->configuration.get_int_distribution_range()[0],
                                                        this->configuration.get_int_distribution_range()[1]},
                     std::uniform_real_distribution<double>{this->configuration.get_real_distribution_range()[0],
                                                            this->configuration.get_real_distribution_range()[1]},
                     this->configuration.get_random_seed()}
{}

void Component::run()
{
    if (const auto& listen_endpoint = configuration.get_listen_endpoint()) {
        async_accept(std::make_shared<TcpAcceptor>(io_context, *listen_endpoint));
    }

    if (!configuration.get_accepting_components().empty()) {
        const auto socket = std::make_shared<TcpSocket>(io_context);
        socket->async_connect(configuration.get_accepting_components()[0].endpoint,
                              [this, socket](const ErrorCode& error) {
                                  if (error) { throw ConnectError{error}; }
                                  on_connect_success(socket, 1);
                              });
    }

    std::this_thread::sleep_for(configuration.get_connect_delay());
    io_context.run();
}

bool Component::write_and_return(bool value, const std::vector<std::string>& components)
{
    const auto serialized_value = serialize(value);
    for (const auto& component : components) { get_connection(component).write(serialized_value); }
    return value;
}

Connection& Component::get_connection(const std::string& component)
{
    const auto it = connections.find(std::string{component});
    if (it == connections.end()) { throw MissingConnectionError{component}; }
    return it->second;
}

void Component::async_accept(std::shared_ptr<TcpAcceptor> acceptor) noexcept
{
    acceptor->async_accept([this, acceptor](const ErrorCode& error, TcpSocket socket) {
        if (error) { throw AcceptError{error}; }
        on_accept_success(acceptor, std::move(socket));
    });
    if (get_observer()) { get_observer()->on_accept_registered(acceptor->local_endpoint()); }
}

void Component::on_accept_success(std::shared_ptr<TcpAcceptor> acceptor, TcpSocket socket)
{
    const auto n_connections_to_accept = configuration.get_connecting_components().size();
    assert(acceptor);
    assert(socket.is_open());
    assert(connections.size() <= n_connections_to_accept);

    if (get_observer()) { get_observer()->on_accept_success(socket.remote_endpoint()); }
    const auto* peer_name = identify_connected_component(socket.remote_endpoint());
    if (!peer_name) {
        if (get_observer()) { get_observer()->on_connection_from_unexpected_endpoint(socket.remote_endpoint()); }
    } else {
        if (get_observer()) { get_observer()->on_connection_identified(*peer_name, socket.remote_endpoint()); }
        Connection connection{std::move(socket)};
        const auto [it, inserted] = connections.try_emplace(*peer_name, std::move(connection));
        if (!inserted && get_observer()) {
            get_observer()->on_multiple_connects_from_same_peer(*peer_name, connection.get_socket().remote_endpoint());
        }
    }

    if (connections.size() < n_connections_to_accept) {
        async_accept(std::move(acceptor));
    } else if (all_connections_established()) {
        on_all_connections_established();
    }
}

void Component::async_connect(std::size_t accepting_component_index) noexcept
{
    assert(accepting_component_index < configuration.get_accepting_components().size());

    const auto& peer_component = configuration.get_accepting_components()[accepting_component_index];
    const auto socket          = std::make_shared<TcpSocket>(io_context);
    socket->async_connect(peer_component.endpoint, [this, socket, accepting_component_index](const ErrorCode& error) {
        if (error) { throw ConnectError{error}; }
        on_connect_success(socket, accepting_component_index);
    });
    if (get_observer()) { get_observer()->on_connect_registered(peer_component.name, peer_component.endpoint); }
}

void Component::on_connect_success(std::shared_ptr<TcpSocket> socket, std::size_t accepting_component_index)
{
    assert(socket && socket->is_open());
    assert(accepting_component_index < configuration.get_accepting_components().size());
    assert(socket->remote_endpoint() == configuration.get_accepting_components()[accepting_component_index].endpoint);

    const auto& peer_component = configuration.get_accepting_components()[accepting_component_index];
    if (get_observer()) { get_observer()->on_connect_success(peer_component.name, socket->remote_endpoint()); }
    [[maybe_unused]] const auto [it, inserted] =
        connections.emplace(peer_component.name, Connection{std::move(*socket)});
    assert(inserted);

    const auto next_accepting_component_index = accepting_component_index + 1;
    if (next_accepting_component_index < configuration.get_accepting_components().size()) {
        async_connect(next_accepting_component_index);
    } else if (all_connections_established()) {
        on_all_connections_established();
    }
}

bool Component::all_connections_established() const noexcept
{
    return connections.size()
           == configuration.get_connecting_components().size() + configuration.get_accepting_components().size();
}

const std::string* Component::identify_connected_component(const TcpEndpoint& peer_endpoint) const
{
    const auto& accepting_components = configuration.get_accepting_components();
    const auto it                    = std::find_if(accepting_components.begin(),
                                 accepting_components.end(),
                                 [&peer_endpoint](const ComponentInfo& component_info) noexcept -> bool {
                                     return component_info.endpoint == peer_endpoint;
                                 });
    return it == accepting_components.end() ? nullptr : &it->name;
}

ComponentObserver* Component::get_observer() const noexcept
{
    return configuration.get_component_observer();
}

MissingConnectionError::MissingConnectionError(std::string_view missing_component) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("missing connection for component {:?}"), missing_component)}
{}

} // namespace maf
