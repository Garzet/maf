#include "maf/options.hpp"

#include "maf/log.hpp"

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <algorithm>
#include <cassert>
#include <charconv>
#include <chrono>

namespace maf {

namespace {

constexpr std::string_view default_observer_name{"log"};

[[nodiscard]] std::unique_ptr<ComponentObserver>
    load_component_observer(const ParsedCommandLineOptions& parsed_command_line_options);

[[nodiscard]] std::optional<unsigned> load_random_seed(const ParsedCommandLineOptions& parsed_command_line_options);

[[nodiscard]] std::chrono::milliseconds load_connect_delay(const ParsedCommandLineOptions& parsed_command_line_options);

[[nodiscard]] std::array<int, 2>
    load_integer_distribution_range(const ParsedCommandLineOptions& parsed_command_line_options);

[[nodiscard]] std::array<double, 2>
    load_real_distribution_range(const ParsedCommandLineOptions& parsed_command_line_options);

} // namespace

ParsedCommandLineOptions::ParsedCommandLineOptions(BasicParsedOptions basic_parsed_options, std::string executable_name)
    : executable_name{std::move(executable_name)}
{
    static constexpr std::size_t positional_options_reserve_size{8};
    flag_options.reserve(basic_parsed_options.options.size());
    argument_options.reserve(basic_parsed_options.options.size());
    positional_options.reserve(positional_options_reserve_size);

    for (auto& option : basic_parsed_options.options) {
        if (option.string_key.empty()) {
            assert(option.value.size() == 1);
            add_positional_option(std::move(option.value.front()));
        } else if (option.value.empty()) {
            add_flag_option(std::move(option.string_key));
        } else if (option.value.size() == 1) {
            add_argument_option(std::move(option.string_key), std::move(option.value.front()));
        } else {
            throw MultiTokenOptionEncounteredError{option.string_key};
        }
    }
}

const std::string& ParsedCommandLineOptions::get_executable_name() const noexcept
{
    return executable_name;
}

bool ParsedCommandLineOptions::is_flag_option_present(std::string_view key) const noexcept
{
    const auto it = std::find_if(flag_options.begin(),
                                 flag_options.end(),
                                 [key](const std::string& flag_option) noexcept { return key == flag_option; });
    return it != flag_options.end();
}

const std::string* ParsedCommandLineOptions::get_argument_option_value(std::string_view key) const noexcept
{
    const auto it =
        std::find_if(argument_options.begin(),
                     argument_options.end(),
                     [key](const std::pair<std::string, std::string>& option) noexcept { return key == option.first; });
    return it != argument_options.end() ? &it->second : nullptr;
}

const std::vector<std::string>& ParsedCommandLineOptions::get_positional_options() const noexcept
{
    return positional_options;
}

void ParsedCommandLineOptions::add_flag_option(std::string key) noexcept
{
    assert(!key.empty());
    flag_options.emplace(std::move(key));
}

void ParsedCommandLineOptions::add_argument_option(std::string key, std::string value) noexcept
{
    assert(!key.empty());
    assert(!value.empty());
    argument_options.emplace(std::move(key), std::move(value));
}

void ParsedCommandLineOptions::add_positional_option(std::string value) noexcept
{
    assert(!value.empty());
    positional_options.emplace_back(std::move(value));
}

CommandLineOptionsDescription::CommandLineOptionsDescription() noexcept
    : options_description{"Options", help_line_length, help_line_length / 2}
{
    namespace po = boost::program_options;

    // clang-format off
    options_description.add_options()
        ("help,h",     "produce help message")
        ("seed,s",     po::value<std::string>()
                          ->value_name("<seed>"),
                       "set random generator seed")
        ("observer,o", po::value<std::string>()
                          ->value_name("<observer>")
                          ->default_value(std::string{default_observer_name}),
                       "set component observer to use generator seed")
        ("delay,d",    po::value<std::string>()
                             ->value_name("<delay>")
                             ->default_value(std::to_string(DynamicConfigurationData::default_connect_delay.count())),
                          "milliseconds to sleep before attempting to connect to other components")
        ("irange,i",  po::value<std::string>()
                          ->value_name("<range>")
                          ->default_value(fmt::format(FMT_STRING("{},{}"),
                                                      DynamicConfigurationData::default_int_distribution_range[0],
                                                      DynamicConfigurationData::default_int_distribution_range[1])),
                      "integer distribution range")
        ("frange,f",  po::value<std::string>()
                          ->value_name("<range>")
                          ->default_value(fmt::format(FMT_STRING("{},{}"),
                                                      DynamicConfigurationData::default_real_distribution_range[0],
                                                      DynamicConfigurationData::default_real_distribution_range[1])),
                      "real distribution range");
    // clang-format on
}

ParsedCommandLineOptions CommandLineOptionsDescription::parse_command_line(int argc, char** argv) const
{
    if (argc < 1) { throw EmptyCommandLineError{}; }
    return ParsedCommandLineOptions{boost::program_options::parse_command_line(argc, argv, options_description),
                                    argv[0]};
}

std::string CommandLineOptionsDescription::format_help_string(std::string_view executable_name) const noexcept
{
    std::ostringstream oss;
    oss << format_usage_string(executable_name) << '\n' << options_description;
    return oss.str();
}

std::string CommandLineOptionsDescription::format_usage_string(std::string_view executable_name) noexcept
{
    return fmt::format(FMT_STRING("USAGE: {} [OPTION]..."), executable_name);
}

DynamicConfigurationData load_dynamic_configuration_data(const ParsedCommandLineOptions& parsed_command_line_options)
{
    return DynamicConfigurationData{load_component_observer(parsed_command_line_options),
                                    load_random_seed(parsed_command_line_options),
                                    load_connect_delay(parsed_command_line_options),
                                    load_integer_distribution_range(parsed_command_line_options),
                                    load_real_distribution_range(parsed_command_line_options)};
}

EmptyCommandLineError::EmptyCommandLineError() noexcept
    : std::runtime_error{"command line does not contain a single argument (argc == 0)"}
{}

MultiTokenOptionEncounteredError::MultiTokenOptionEncounteredError(std::string_view key) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("multiple tokens parsed for value of option {:?}"), key)}
{}

InvalidObserverNameError::InvalidObserverNameError(std::string_view observer_name,
                                                   const std::vector<std::string_view>& available_observers) noexcept
    : std::runtime_error{fmt::format(
        FMT_STRING("invalid observer name {:?} (availabe observers: {}"), observer_name, available_observers)}
{
    assert(std::find(available_observers.cbegin(), available_observers.cend(), observer_name)
           == available_observers.cend());
}

InvalidSeedValueError::InvalidSeedValueError(std::string_view seed_value) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("value {:?} is not a valid seed (must be unsigned value)"), seed_value)}
{}

InvalidConnectDelayValueError::InvalidConnectDelayValueError(std::string_view connect_delay_value) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("value {:?} is not a valid connect delay (must be unsigned value)"),
                                     connect_delay_value)}
{}

InvalidIntDistributionRangeStringError::InvalidIntDistributionRangeStringError(std::string_view range_string) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("string {:?} is not a valid integer distribution range"), range_string)}
{}

InvalidRealDistributionRangeStringError::InvalidRealDistributionRangeStringError(std::string_view range_string) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("string {:?} is not a valid real distribution range"), range_string)}
{}

namespace {

std::unique_ptr<ComponentObserver> load_component_observer(const ParsedCommandLineOptions& parsed_command_line_options)
{
    using Factory =
        std::unique_ptr<ComponentLogObserver> (*)(const ParsedCommandLineOptions& parsed_command_line_options) noexcept;
    struct NamedObserverFactory final
    {
        std::string_view name;
        Factory factory;
    };

    static constexpr std::array<NamedObserverFactory, 2> factories = {
        NamedObserverFactory{"null",
                             [](const ParsedCommandLineOptions& /*parsed_command_line_options*/) noexcept
                             -> std::unique_ptr<ComponentLogObserver> { return nullptr; }},
        NamedObserverFactory{
            "log",
            [](const ParsedCommandLineOptions& /*parsed_command_line_options*/) noexcept
            -> std::unique_ptr<ComponentLogObserver> { return std::make_unique<ComponentLogObserver>(); }},
    };

    const auto* const observer = parsed_command_line_options.get_argument_option_value("observer");
    std::string_view observer_name{observer ? std::string_view{*observer} : default_observer_name};
    const auto* const it = std::find_if(factories.cbegin(),
                                        factories.cend(),
                                        [observer_name](const NamedObserverFactory& named_factory) noexcept -> bool {
                                            return observer_name == named_factory.name;
                                        });
    if (it == factories.end()) {
        std::vector<std::string_view> available_observer_names;
        available_observer_names.reserve(factories.size());
        for (const auto& named_factory : factories) { available_observer_names.push_back(named_factory.name); }
        throw InvalidObserverNameError{observer_name, available_observer_names};
    }
    return it->factory(parsed_command_line_options);
}

std::optional<unsigned> load_random_seed(const ParsedCommandLineOptions& parsed_command_line_options)
{
    const auto* const seed = parsed_command_line_options.get_argument_option_value("seed");
    if (!seed) { return std::nullopt; }
    unsigned result{};
    const auto [ptr, ec] = std::from_chars(seed->c_str(), seed->c_str() + seed->size(), result);
    if (ec != std::errc{} || ptr != seed->c_str() + seed->size()) { throw InvalidSeedValueError{*seed}; }
    return result;
}

std::chrono::milliseconds load_connect_delay(const ParsedCommandLineOptions& parsed_command_line_options)
{
    const auto* const delay = parsed_command_line_options.get_argument_option_value("delay");
    if (!delay) { return DynamicConfigurationData::default_connect_delay; }
    unsigned result{};
    const auto [ptr, ec] = std::from_chars(delay->c_str(), delay->c_str() + delay->size(), result);
    if (ec != std::errc{} || ptr != delay->c_str() + delay->size()) { throw InvalidConnectDelayValueError{*delay}; }
    return std::chrono::milliseconds{result};
}

std::array<int, 2> load_integer_distribution_range(const ParsedCommandLineOptions& parsed_command_line_options)
{
    const auto* const irange = parsed_command_line_options.get_argument_option_value("irange");
    if (!irange) { return DynamicConfigurationData::default_int_distribution_range; }
    const auto comma_index = irange->find(',');
    if (comma_index == std::string::npos) { throw InvalidIntDistributionRangeStringError{*irange}; }

    std::array<int, 2> result{};

    {
        const std::string_view lower_str{irange->c_str(), comma_index};
        const auto [ptr, ec] = std::from_chars(lower_str.data(), lower_str.data() + lower_str.size(), result[0]);
        if (ec != std::errc{} || ptr != lower_str.data() + lower_str.size()) {
            throw InvalidIntDistributionRangeStringError{*irange};
        }
    }
    {
        if (comma_index == irange->size() - 1) { throw InvalidIntDistributionRangeStringError{*irange}; }
        const auto upper_str_size = irange->size() - comma_index - 1;
        const std::string_view upper_str{irange->c_str() + comma_index + 1, upper_str_size};
        const auto [ptr, ec] = std::from_chars(upper_str.data(), upper_str.data() + upper_str.size(), result[1]);
        if (ec != std::errc{} || ptr != upper_str.data() + upper_str.size()) {
            throw InvalidIntDistributionRangeStringError{*irange};
        }
    }

    return result;
}

std::array<double, 2> load_real_distribution_range(const ParsedCommandLineOptions& parsed_command_line_options)
{
    const auto* const frange = parsed_command_line_options.get_argument_option_value("irange");
    if (!frange) { return DynamicConfigurationData::default_real_distribution_range; }
    const auto comma_index = frange->find(',');
    if (comma_index == std::string::npos) { throw InvalidRealDistributionRangeStringError{*frange}; }

    std::array<double, 2> result{};

    {
        const std::string_view lower_str{frange->c_str(), comma_index};
        const auto [ptr, ec] = std::from_chars(lower_str.data(), lower_str.data() + lower_str.size(), result[0]);
        if (ec != std::errc{} || ptr != lower_str.data() + lower_str.size()) {
            throw InvalidRealDistributionRangeStringError{*frange};
        }
    }
    {
        if (comma_index == frange->size() - 1) { throw InvalidRealDistributionRangeStringError{*frange}; }
        const auto upper_str_size = frange->size() - comma_index - 1;
        const std::string_view upper_str{frange->c_str() + comma_index + 1, upper_str_size};
        const auto [ptr, ec] = std::from_chars(upper_str.data(), upper_str.data() + upper_str.size(), result[1]);
        if (ec != std::errc{} || ptr != upper_str.data() + upper_str.size()) {
            throw InvalidRealDistributionRangeStringError{*frange};
        }
    }

    return result;
}

} // namespace

} // namespace maf
