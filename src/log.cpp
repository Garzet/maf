#include "maf/log.hpp"

#include <fmt/format.h>

namespace maf {

void ComponentLogObserver::on_accept_registered(const TcpEndpoint& listen_endpoint) noexcept
{
    fmt::print(FMT_STRING("listening on endpoint {}\n"), listen_endpoint);
}

void ComponentLogObserver::on_accept_success(const TcpEndpoint& peer_endpoint) noexcept
{
    fmt::print(FMT_STRING("successfuly accepted connection from {}\n"), peer_endpoint);
}

void ComponentLogObserver::on_connection_from_unexpected_endpoint(const TcpEndpoint& peer_endpoint) noexcept
{
    fmt::print(FMT_STRING("connection received from unexpected endpoint {}\n"), peer_endpoint);
}

void ComponentLogObserver::on_connection_identified(const std::string& peer_name,
                                                    const TcpEndpoint& peer_endpoint) noexcept
{
    fmt::print(FMT_STRING("peer at {} identified as {:?}\n"), peer_endpoint, peer_name);
}

void ComponentLogObserver::on_multiple_connects_from_same_peer(const std::string& peer_name,
                                                               const TcpEndpoint& peer_endpoint) noexcept
{
    fmt::print(FMT_STRING("multiple connections from peer at {} identified as {:?}\n"), peer_endpoint, peer_name);
}

void ComponentLogObserver::on_connect_registered(const std::string& peer_name,
                                                 const TcpEndpoint& peer_endpoint) noexcept
{
    fmt::print(FMT_STRING("connect to peer {:?} at {} registered"), peer_name, peer_endpoint);
}

void ComponentLogObserver::on_connect_success(const std::string& peer_name, const TcpEndpoint& peer_endpoint) noexcept
{
    fmt::print(FMT_STRING("successfuly connected to to peer {:?} at {}"), peer_name, peer_endpoint);
}

} // namespace maf
