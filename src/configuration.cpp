#include "maf/configuration.hpp"

#include <fmt/format.h>

namespace maf {

Configuration::Configuration(StaticConfigurationData static_config_data, DynamicConfigurationData dynamic_config_data)
    : static_config_data{std::move(static_config_data)}, dynamic_config_data{std::move(dynamic_config_data)}
{
    validate();
}

const std::string& Configuration::get_component_name() const noexcept
{
    return static_config_data.component_name;
}

const std::optional<TcpEndpoint>& Configuration::get_listen_endpoint() const noexcept
{
    return static_config_data.listen_endpoint;
}

const std::vector<ComponentInfo>& Configuration::get_connecting_components() const noexcept
{
    return static_config_data.connecting_components;
}

const std::vector<ComponentInfo>& Configuration::get_accepting_components() const noexcept
{
    return static_config_data.accepting_components;
}

std::optional<unsigned> Configuration::get_random_seed() const noexcept
{
    return dynamic_config_data.random_seed;
}

std::array<int, 2> Configuration::get_int_distribution_range() const noexcept
{
    return dynamic_config_data.int_distribution_range;
}

std::array<double, 2> Configuration::get_real_distribution_range() const noexcept
{
    return dynamic_config_data.real_distribution_range;
}

std::chrono::milliseconds Configuration::get_connect_delay() const noexcept
{
    return dynamic_config_data.connect_delay;
}

ComponentObserver* Configuration::get_component_observer() const noexcept
{
    return dynamic_config_data.component_observer.get();
}

void Configuration::validate() const
{
    const auto& accepting_components  = get_accepting_components();
    const auto& connecting_components = get_connecting_components();

    { // Ensure no empty component names.
        if (get_component_name().empty()) { throw EmptyComponentNameError{}; }
        for (const auto& cc : connecting_components) {
            if (cc.name.empty()) { throw EmptyComponentNameError{}; }
        }
        for (const auto& ac : accepting_components) {
            if (ac.name.empty()) { throw EmptyComponentNameError{}; }
        }
    }

    { // Ensure no duplicate component names or duplicate endpoints.
        if (!accepting_components.empty()) {
            for (auto i = accepting_components.begin(); i != accepting_components.end() - 1; ++i) {
                for (auto j = i + 1; j != accepting_components.end(); ++j) {
                    if (i->name == j->name) { throw DuplicateComponentNameError{i->name}; }
                    if (i->endpoint == j->endpoint) { throw DuplicateComponentEndpointError{i->endpoint}; }
                }
            }
        }
        if (!connecting_components.empty()) {
            for (auto i = connecting_components.begin(); i != connecting_components.end() - 1; ++i) {
                for (auto j = i + 1; j != connecting_components.end(); ++j) {
                    if (i->name == j->name) { throw DuplicateComponentNameError{i->name}; }
                    if (i->endpoint == j->endpoint) { throw DuplicateComponentEndpointError{i->endpoint}; }
                }
            }
        }

        for (const auto& ac : accepting_components) {
            for (const auto& cc : connecting_components) {
                if (ac.name == cc.name) { throw DuplicateComponentNameError{ac.name}; }
                if (ac.endpoint == cc.endpoint) { throw DuplicateComponentEndpointError{ac.endpoint}; }
            }
            if (ac.name == get_component_name()) { throw DuplicateComponentNameError{ac.name}; }
            if (ac.endpoint == get_listen_endpoint()) { throw DuplicateComponentEndpointError{ac.endpoint}; }
        }

        for (const auto& cc : connecting_components) {
            if (cc.name == get_component_name()) { throw DuplicateComponentNameError{cc.name}; }
            if (cc.endpoint == get_listen_endpoint()) { throw DuplicateComponentEndpointError{cc.endpoint}; }
        }
    }

    // If connecting components are provided, ensure listen endpoint is provided.
    if (!connecting_components.empty() && !get_listen_endpoint()) { throw MissingListenEndpointError{}; }

    // Validate distribution ranges.
    if (dynamic_config_data.int_distribution_range[0] >= dynamic_config_data.int_distribution_range[1]) {
        throw InvalidIntDistributionRangeError{dynamic_config_data.int_distribution_range[0],
                                               dynamic_config_data.int_distribution_range[1]};
    }
    if (dynamic_config_data.real_distribution_range[0] > dynamic_config_data.real_distribution_range[1]) {
        throw InvalidRealDistributionRangeError{dynamic_config_data.real_distribution_range[0],
                                                dynamic_config_data.real_distribution_range[1]};
    }
}

EmptyComponentNameError::EmptyComponentNameError() noexcept
    : std::runtime_error{"empty string is not a valid component name"}
{}

DuplicateComponentNameError::DuplicateComponentNameError(std::string_view duplicate_name) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("multiple components named {:?}"), duplicate_name)}
{}

DuplicateComponentEndpointError::DuplicateComponentEndpointError(const TcpEndpoint& duplicate_endpoint) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("multiple components with endpoint {}:{}"),
                                     duplicate_endpoint.address().to_string(),
                                     duplicate_endpoint.port())}
{}

InvalidIntDistributionRangeError::InvalidIntDistributionRangeError(int lower, int upper) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("invalid int distribution range [{}, {}]"), lower, upper)}
{
    assert(lower >= upper);
}

InvalidRealDistributionRangeError::InvalidRealDistributionRangeError(double lower, double upper) noexcept
    : std::runtime_error{fmt::format(FMT_STRING("invalid real distribution range [{}, {}>"), lower, upper)}
{
    assert(lower >= upper);
}

MissingListenEndpointError::MissingListenEndpointError() noexcept : std::runtime_error{"listen endpoint not provided"}
{}

} // namespace maf
